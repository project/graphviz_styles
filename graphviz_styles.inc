<?php

/**
 * @file
 * contains functions related to editing
 * Graphviz graph, node and edge styles.
 */

/**
 * JSON callback for generating AHAH style previews.
 */
function graphviz_styles_json_render($type) {
  $style = $_GET;
  _graphviz_styles_clean_style_array($style);
  _graphviz_styles_collapse_style_array($style);
  $dot = _graphviz_styles_generate_preview_code($type, $style, 'canviz');
  drupal_json(array(
    'status' => TRUE,
    'data' => graphviz_filter_process($dot),
  ));
}

/**
 * Transforms an array as returned by a style form into a string array
 * that the Image_GraphViz will digest.
 */
function _graphviz_styles_collapse_style_array(&$array) {
  foreach ($array as $name => $value)
    if (is_array($value))
      $array[$name] = implode(',', $value);
    elseif (is_bool($value))
      $array[$name] = $value ? 'true' : 'false';
    elseif (is_numeric($value))
      $array[$name] = (string) $value;
}

/**
 * Removes bogus keys from style array (cosmetics).
 */
function _graphviz_styles_clean_style_array(&$style) {
  foreach (array('q', 'name', 'id', 'op', 'submit', 'preview', 'reset', 'delete', 'form_build_id', 'form_token', 'form_id') as $key) {
    unset($style[$key]);
  }
}

/**
 * Generates header code which defines the render algorithm and output format.
 */
function _graphviz_styles_generate_header_code($formats, $command = 'dot', $imagemap = TRUE) {
  return "/*\n".
         " * @command = $command\n".
         " * @formats = $formats\n".
         ' * @imagemap = '. ($imagemap ? 'TRUE' : 'FALSE') ."\n".
         " */\n";
}

/**
 * Generates DOT code for preview of graph/node/edge styles.
 */
function _graphviz_styles_generate_preview_code($type, $style, $formats, $scale = 1) {
  require_once('Image/GraphViz.php');
  $preview_graphstyle = array(
    'bgcolor' => '#ffffff00', /* transparent background */
    'rankdir' => 'LR',
  );
  $hidden_nodestyle = array(
    'style' => 'invis',
    'height' => '0.4',
    'width' => '0',
    'fixedsize' => 'true',
  );
  $mini_nodestyle = array(
    'style' => 'dashed',
    'color' => '#ededed',
    'fontsize' => '0',
    'height' => '0.3',
    'width' => '0.3',
    'fixedsize' => 'true',
  );
  $default_edgestyle = array(
    'color' => '#c0c0c0',
  );

  _graphviz_styles_collapse_style_array($style);

  switch ($type) {
    case 'graph':
      $graph = new Image_GraphViz(TRUE, $style);
      $graph->addCluster('cluster1', 'cluster 1');
      $graph->addCluster('cluster2', 'cluster 2');
      $graph->addNode('1', $mini_nodestyle, 'cluster1');
      $graph->addNode('2', $mini_nodestyle, 'cluster1');
      $graph->addNode('3', $mini_nodestyle, 'cluster1');
      $graph->addNode('4', $mini_nodestyle, 'cluster2');
      $graph->addNode('5', $mini_nodestyle, 'cluster2');
      $graph->addEdge(array('1' => '2'), $default_edgestyle);
      $graph->addEdge(array('2' => '3'), $default_edgestyle);
      $graph->addEdge(array('3' => '4'), $default_edgestyle);
      $graph->addEdge(array('4' => '2'), $default_edgestyle);
      $graph->addEdge(array('5' => '4'), $default_edgestyle);
      $graph->addEdge(array('1' => '3'), $default_edgestyle);
      break;
    case 'node':
      $graph = new Image_GraphViz(TRUE, $preview_graphstyle);
      $graph->addNode('Style Preview', $style);
      break;
    case 'edge':
      $graph = new Image_GraphViz(TRUE, $preview_graphstyle);
      $graph->addNode('n1', $hidden_nodestyle);
      $graph->addNode('n2', $hidden_nodestyle);
      $graph->addEdge(array('n1' => 'n2'), array_merge($style, array('minlen' => '3', 'dir' => 'both')));
  }
  $dot = _graphviz_styles_generate_header_code($formats, 'dot', FALSE) . $graph->parse();
  return $dot;
}

/**
 * Writes a graphviz style to the database.
 *
 * @param $type
 *   'graph', 'node' or 'edge'
 * @param $name
 *   name of the style
 * @param $style
 *   the style data itself
 * @param $id
 *   if a style is updated, its id
 *
 * @return
 *   the id of the saved/updated style on success or FALSE on failure
 */
function graphviz_styles_save_style($type, $name, $style, $id = 0) {
  _graphviz_styles_clean_style_array($style);
  $style_record = array(
    'id' => is_numeric($id) ? $id : 0,
    'name' => $name,
    'type' => $type,
    'data' => $style,
  );
  if (drupal_write_record('graphviz_styles', $style_record, $style_record['id'] ? array('id') : array())) {
    return $style_record['id'];
  }
  return FALSE;
}

/**
 * Loads a style from the database or returns a default one.
 *
 * @param $type
 *   type of the style requested
 * @param $id
 *   either unique style ID or 'new'
 *
 * @return
 *   loaded $style array on success or empty array on failure?
 */
function graphviz_styles_load_style($type, $id) {
  if (is_numeric($id)) {
    $style_record = db_fetch_object(db_query("SELECT name, data from {graphviz_styles} WHERE id = %d AND type ='%s'", $id, $type));
    if ($style_record) {
      $style_record = (array) drupal_unpack($style_record);
      unset($style_record['data']);
      return $style_record;
    }
    else {
      return array();
    }
  }
  else {
    switch ($type) {
      case 'graph':
        return array(
          'bgcolor' => '#ffffff',
          'color' => '#000000',
          'fillcolor' => '#a0a0a0',
          'style' => array('filled'),
          'label' => 'Graph label',
          'labeljust' => 'c',
          'labelloc' => 't',
          'fontname' => 'serif',
          'fontcolor' => '#000000',
          'fontsize' => 12,
          'rankdir' => 'TB',
        );
      case 'node':
        return array(
          'color' => '#000000',
          'fillcolor' => '#0f0f0f',
          'shape' => 'box',
          'style' => array(),
          'fontname' => 'serif',
          'fontcolor' => '#000000',
          'fontsize' => 12,
        );
      case 'edge':
        return array(
          'color' => '#000000',
          'style' => array(),
          'arrowhead' => 'normal',
          'arrowtail' => 'none',
          'arrowsize' => '1',
          'decorate' => FALSE,
          'label' => 'Label',
          'fontname' => 'serif',
          'fontcolor' => '#000000',
          'fontsize' => 12,
        );
    }
  }
}

/**
 * Removes a style identified by its ID from the database.
 */
function graphviz_styles_delete_style($id, $type) {
  if (db_query('DELETE from {graphviz_styles} WHERE id = %d', $id)) {
    drupal_set_message(t('Deleted !type style #!id.', array('!type' => $type, '!id' => $id)));
  }
}

/**
 * Displays an overview of all existing Graphviz styles and CRUD operations.
 */
function graphviz_styles_list_page() {
  $output = '<br />';
  $header = array(t('ID'), t('Name'), t('Preview'), array('data' => t('Operations'), 'colspan' => 3));
  foreach (array('graph', 'node', 'edge') as $type) {
    $output .= '<h3>'. t('!type Styles', array('!type' => drupal_ucfirst($type))) .' ('. l(t('Create'), "admin/build/graphviz_styles/$type/new") .')</h3>';
    $rows = array();
    $result = db_query("SELECT id from {graphviz_styles} WHERE type = '%s'", $type);
    while ($id = db_result($result)) {
      $style = graphviz_styles_load_style($type, $id);
      $preview = graphviz_filter_process(_graphviz_styles_generate_preview_code($type, $style, 'png'));
      $rows[] = array(
        $id,
        $style['name'],
        $preview,
        l(t('Edit'), "admin/build/graphviz_styles/$type/$id"),
        l(t('Clone'), "admin/build/graphviz_styles/$type/$id/clone"),
        l(t('Delete'), "admin/build/graphviz_styles/$type/$id/delete"),
      );
    }
    $output .= theme('table', $header, $rows);
  }
  return $output;
}

/**
 * Inserts style elements into form.
 *
 * @param $form_state
 * @param $type
 *   type for which specific options are added, one of
 *   - 'graph'
 *   - 'node'
 *   - 'edge'
 * @param $id
 * @param $op
 *   operation: edit, delete or clone
 */
function graphviz_styles_style_form(&$form_state, $type, $id, $op = 'edit') {
  // Refuse fusked ID parameter.
  if (!is_numeric($id) && $id !== 'new') {
    drupal_goto('admin/build/graphviz_styles/list');
  }

  $style = graphviz_styles_load_style($type, $id);
  // If a style cannot be found with the given ID and a new one hasn't been
  // submitted, show 404 error.
  if (!$style && empty($form_state['post'])) {
    drupal_not_found();
    exit;
  }

  // Breadcrumb !
  drupal_set_breadcrumb(array(l(t('Home'), NULL), l(t('Graphviz Styles'), 'admin/build/graphviz_styles/list')));

  // If style deletion is requested, show a confirmation form.
  if ($op == 'delete') {
    $form = array();
    return confirm_form(array(), t('Do you really want to delete the !type style #!id "@name"?',
                        array('!type' => $type, '!id' => $id, '@name' => $style['name'])), "admin/build/graphviz_styles/$type/$id", NULL, t('Delete'));
  }

  $path = GRAPHVIZ_STYLES_PATH;

  // Include specific CSS & JS files.
  drupal_add_css($path .'/graphviz_styles.css');
  drupal_add_js($path .'/jquery-spin/jquery-spin.js');
  drupal_add_js($path .'/graphviz_styles.js');
  if (function_exists('graphviz_filter_add_canviz_js')) {
    graphviz_filter_add_canviz_js();
  }

  // Add correct JSON path to Drupal JS settings.
  drupal_add_js(array(
    'graphviz_styles' => array(
      'json_path' => url("graphviz_style_render_js/$type"),
      'img_path' => "/$path/jquery-spin/",
    )), 'setting');

  $form = array(
    '#type' => 'fieldset',
    '#title' => t('Custom Style'),
    '#attributes' => array('class' => 'graphviz-style-wrapper'),
    '#collapsible' => TRUE,
    'name' => array(
      '#type' => 'textfield',
      '#title' => t('Style Name'),
      '#weight' => -2,
    ),
  );

  // Set an adequate page title.
  if ($id == 'new') {
    $operation = t('Creating new');
  }
  else {
    if ($op == 'clone') {
      $operation = t('Cloning');
      // Let's not overwrite the existing style..
      $id = 'new';
    }
    else {
      $operation = t('Editing');
    }
  }
  drupal_set_title(t('!operation !type style @name', array(
    '!operation' => $operation,
    '!type' => $type,
    '@name' => empty($style['name']) ? '' : '"'. $style['name'] .'"')));

  // Merge specific style options into form.
  call_user_func_array('_graphviz_styles_'. $type .'_style_form', array(&$form));

  // Operate on submitted style array if set.
  $style = empty($form_state['post']) ? $style : $form_state['post'];

  // Load the style into the form's default values.
  foreach ($form as $key => $value) {
    if (is_array($value) && isset($style[$key])) {
      $form[$key]['#default_value'] = $style[$key];
    }
  }

  // Add common style form elements.
  $form['style-preview'] = array(
    '#type' => 'markup',
    '#value' => '<div class="graphviz-style-preview">'.
      graphviz_filter_process(_graphviz_styles_generate_preview_code($type, $style, 'png')) .'</div>',
    '#weight' => -1,
  );
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );
  $form['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#prefix' => '<div class="graphviz-widget-clear-float"></div>',
  );
  if (is_numeric($id) && $id > 0) {
    $form['saveasnew'] = array(
      '#type' => 'submit',
      '#value' => t('Save as new'),
    );
  }
  $form['preview'] = array(
    '#type' => 'submit',
    '#value' => t('Preview'),
  );
  $form['reset'] = array(
    '#type' => 'submit',
    '#value' => t('Reset'),
  );
  if (is_numeric($id) && $id > 0) {
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
  }

  return $form;
}

/**
 * Validate handler for style editing form.
 */
function graphviz_styles_style_form_validate($form, &$form_state) {
  switch ($form_state['values']['op']) {
    case t('Save as new'):
    case t('Save'):
      if (empty($form_state['values']['name'])) {
        form_set_error('name', t('A name <strong>has</strong> to be supplied for each style.'));
      }
      break;
    case t('Delete'):
      $type = arg(3);
      $id = arg(4);
      if (arg(5) != 'delete') {
        drupal_goto("admin/build/graphviz_styles/$type/$id/delete");
      }
      break;
  }
}

/**
 * Submit handler for style editing form.
 */
function graphviz_styles_style_form_submit($form, &$form_state) {
  $type = arg(3);
  switch ($form_state['values']['op']) {
    case t('Save as new'):
      $form_state['values']['id'] = 'new';
    case t('Save'):
      $style = $form_state['values'];
      $id = $style['id'];
      $name = $style['name'];
      if ($id = graphviz_styles_save_style($type, $name, $style, $id)) {
        drupal_set_message(t('Graphviz !type style "@name" has been saved with ID !id.',
          array('!type' => $type, '@name' => $name, '!id' => $id)));
        $form_state['redirect'] = "admin/build/graphviz_styles/list";
      }
      break;
    case t('Preview'):
      $form_state['redirect'] = FALSE;
      break;
    case t('Reset'):
      $form_state['redirect'] = request_uri();
      break;
    case t('Delete'):
      $id = arg(4);
      graphviz_styles_delete_style($id, $type);
      drupal_goto('admin/build/graphviz_styles/list');
  }
}

/**
 * Inserts graph style specific elements into form.
 *
 * @param $form
 *   (sub)form to be altered
 */
function _graphviz_styles_graph_style_form(&$form) {
  $form['bgcolor'] = array(
    '#type' => 'colorpicker_textfield',
    '#title' => t('Graph background color'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['rankdir'] = array(
    '#type' => 'radios',
    '#title' => t('Rank layouting direction'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#options' => array(
      'TB' => t('top to bottom'),
      'LR' => t('left to right'),
    ),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['color'] = array(
    '#type' => 'colorpicker_textfield',
    '#title' => t('Cluster Outline color'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#prefix' => '<div class="graphviz-widget-float-left graphviz-widget-clear-float">',
    '#suffix' => '</div>',
  );
  $form['style'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Cluster Style'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#options' => array(
      'bold' => t('bold'),
      'dashed' => t('dashed'),
      'dotted' => t('dotted'),
      'filled' => t('filled'),
      'rounded' => t('rounded'),
    ),
    '#multiple' => TRUE,
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['fillcolor'] = array(
    '#type' => 'colorpicker_textfield',
    '#title' => t('Cluster fill color'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Graph label'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#size' => 25,
    '#prefix' => '<div class="graphviz-widget-float-left graphviz-widget-clear-float">',
    '#suffix' => '</div>',
  );
  $form['fontname'] = array(
    '#type' => 'radios',
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#title' => t('Font Style'),
    '#options' => array(
      'serif' => 'serif',
      'sans-serif' => 'sans-serif',
      'monospace' => 'monospace',
    ),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['fontsize'] = array(
    '#type' => 'textfield',
    '#attributes' => array('class' => 'graphviz-style-widget spinbutton'),
    '#title' => t('Font Size'),
    '#size' => 3,
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['fontcolor'] = array(
    '#type' => 'colorpicker_textfield',
    '#title' => t('Font Color'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['labelloc'] = array(
    '#type' => 'radios',
    '#title' => t('Label location'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#options' => array(
      't' => t('top'),
      'b' => t('bottom'),
    ),
    '#prefix' => '<div class="graphviz-widget-float-left graphviz-widget-clear-float">',
    '#suffix' => '</div>',
  );
  $form['labeljust'] = array(
    '#type' => 'radios',
    '#title' => t('Label justification'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#options' => array(
      'l' => t('left'),
      'c' => t('center'),
      'r' => t('right'),
    ),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
}

/**
 * Inserts node style specific elements into form.
 *
 * @param $form
 *   (sub)form to be altered
 */
function _graphviz_styles_node_style_form(&$form) {
  $form['color'] = array(
    '#type' => 'colorpicker_textfield',
    '#title' => t('Border Color'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['shape'] = array(
    '#type' => 'select',
    '#title' => t('Node Shape'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#options' => array(
      'box' => t('Box'),
      'box3d' => t('3D Box'),
      'circle' => t('Circle'),
      'component' => t('Component'),
      'doublecircle' => t('Double circle'),
      'diamond' => t('Diamond'),
      'egg' => t('Egg'),
      'ellipse' => t('Ellipse'),
      'folder' => t('Folder'),
      'hexagon' => t('Hexagon'),
      'house' => t('House'),
      'invtriangle' => t('Invtriangle'),
      'invtrapezium' => t('Invtrapezium'),
      'octagon' => t('Octagon'),
      'note' => t('Note'),
      'parallelogram' => t('Parallelogram'),
      'plaintext' => t('Plain text'),
      'point' => t('Point'),
      'polygon' => t('Polygon'),
      'tab' => t('Tab'),
      'trapezium' => t('Trapezium'),
      'triangle' => t('Triangle'),
      'tripleoctagon' => t('Tripleoctagon'),
    ),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['style'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Style'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#options' => array(
      'bold' => t('bold'),
      'dashed' => t('dashed'),
      'dotted' => t('dotted'),
      'filled' => t('filled'),
      'rounded' => t('rounded'),
      'diagonals' => t('diagonals'),
      'invis' => t('invisible'),
    ),
    '#multiple' => TRUE,
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['fillcolor'] = array(
    '#type' => 'colorpicker_textfield',
    '#title' => t('Fill Color'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['fontname'] = array(
    '#type' => 'radios',
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#title' => t('Font Style'),
    '#options' => array(
      'serif' => 'serif',
      'sans-serif' => 'sans-serif',
      'monospace' => 'monospace',
    ),
    '#prefix' => '<div class="graphviz-widget-float-left graphviz-widget-clear-float">',
    '#suffix' => '</div>',
  );
  $form['fontsize'] = array(
    '#type' => 'textfield',
    '#attributes' => array('class' => 'graphviz-style-widget spinbutton'),
    '#title' => t('Font Size'),
    '#size' => 3,
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['fontcolor'] = array(
    '#type' => 'colorpicker_textfield',
    '#title' => t('Font Color'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
}

/**
 * Inserts edge style specific elements into form.
 *
 * @param $form
 *   (sub)form to be altered
 */
function _graphviz_styles_edge_style_form(&$form) {
  $arrow_shapes = array(
    'none' => t('None'),
    'normal' => t('Default arrow'),
    'empty' => t('Empty arrow'),
    'inv' => t('Inverted arrow'),
    'invempty' => t('Inverted empty arrow'),
    'odot' => t('Empty dot'),
    'dot' => t('Filled dot'),
    'invdot' => t('Inverted arrow with filled dot'),
    'invodot' => t('Inverted arrow with empty dot'),
    'tee' => t('Tee'),
    'diamond' => t('Diamond shape'),
    'odiamond' => t('Empty diamond shape'),
    'diamond' => t('Diamond shape'),
    'crow' => t('Talon shape'),
    'box' => t('Box shape'),
    'obox' => t('Empty box shape'),
    'open' => t('Open Arrow'),
    'halfopen' => t('Half open arrow'),
  );

  $form['style'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Style'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#options' => array(
      'bold' => t('bold'),
      'dashed' => t('dashed'),
      'dotted' => t('dotted'),
      'invis' => t('invisible'),
    ),
    '#multiple' => TRUE,
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['color'] = array(
    '#type' => 'colorpicker_textfield',
    '#title' => t('Color'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['arrowhead'] = array(
    '#type' => 'select',
    '#title' => t('Head arrow shape'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#options' => $arrow_shapes,
    '#prefix' => '<div class="graphviz-widget-float-left graphviz-widget-clear-float">',
    '#suffix' => '</div>',
  );
  $form['arrowtail'] = array(
    '#type' => 'select',
    '#title' => t('Tail arrow shape'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#options' => $arrow_shapes,
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['arrowsize'] = array(
    '#type' => 'textfield',
    '#title' => t('Arrow size'),
    '#size' => 3,
    '#attributes' => array('class' => 'graphviz-style-widget spinbutton'),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Edge label'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#size' => 25,
    '#prefix' => '<div class="graphviz-widget-float-left graphviz-widget-clear-float">',
  );
  $form['decorate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Underline edge label'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#suffix' => '</div>',
  );
  $form['fontname'] = array(
    '#type' => 'radios',
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#title' => t('Font Style'),
    '#options' => array(
      'serif' => 'serif',
      'sans-serif' => 'sans-serif',
      'monospace' => 'monospace',
    ),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['fontsize'] = array(
    '#type' => 'textfield',
    '#attributes' => array('class' => 'graphviz-style-widget spinbutton'),
    '#title' => t('Font Size'),
    '#size' => 3,
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
  $form['fontcolor'] = array(
    '#type' => 'colorpicker_textfield',
    '#title' => t('Font Color'),
    '#attributes' => array('class' => 'graphviz-style-widget'),
    '#prefix' => '<div class="graphviz-widget-float-left">',
    '#suffix' => '</div>',
  );
}
