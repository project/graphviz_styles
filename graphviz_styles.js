
Drupal.graphvizStyles = Drupal.graphvizStyles || {};

Drupal.behaviors.graphvizStyleFormMagic = function (context) {
  $('.spinbutton#edit-arrowsize:not(.spinbutton-processed)').spin({
    max: 20,
    min: 0,
    interval: 0.1,
    timeInterval: 70,
    imageBasePath:Drupal.settings.graphviz_styles.img_path}).addClass('spinbutton-processed');
  $('.spinbutton:not(.spinbutton-processed)').spin({
    max: 60,
    min: 0,
    timeInterval: 70,
    imageBasePath:Drupal.settings.graphviz_styles.img_path}).addClass('spinbutton-processed');
  if (typeof Drupal.settings.graphviz_filter != 'undefined') {
    $('.graphviz-style-widget').change(function() {
      if (Drupal.graphvizStyles.timer) {
        clearTimeout(Drupal.graphvizStyles.timer);
      }
      var widget = $(this);
      var wrapper = widget.closest('.graphviz-style-wrapper');
      var url = Drupal.settings.graphviz_styles.json_path;
      Drupal.graphvizStyles.timer = setTimeout(function(){
        $.getJSON(url, wrapper.find('.graphviz-style-widget').serialize(), function(data) {
          wrapper.find('.graphviz-style-preview').html(data.data);
          Drupal.attachBehaviors();
        });
      }, 400);
      return false;
    });
  }
}
